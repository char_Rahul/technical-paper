# OSI Model
OSI stands for Open Source Interconnection. It was developed by ISO - 'International Organisation of Standardization', in 1984. It is a conceptual model that describes the functioning of a networking system.
 
It consists of 7 layers that work together to transmit data from one machine to another across the globe through internet.

---
## Layers

1. Application Layer
2. Presentation Layer
3. Session Layer
4. Transport Layer
5. Network Layer
6. Data Link Layer
7. Physical Layer

![OSI Layers](https://media.geeksforgeeks.org/wp-content/uploads/computer-network-osi-model-layers.png)

### Application Layer
The first layer of the __OSI Model__ or the first layer from the sender's side to be precise. It is implemented by the network applications. These applications produce the data, which has to be transmitted over the network. Web browsers are example of network applications. These applications do not reside in the application layer, but it uses application layer protocols like HTTP and HTTPs to do web surfing. There are dozens of protocols in the application layer. Few of the protocols include FTP that is used for file transfer, SMTP for emails, Telnet for virtual terminals.

The functions of the Application Layer are:
1. Network Virtual Terminal
2. FTAM(File Transfer Access and Management)
3. Mail Services
4. Directory Services

### Presentation Layer
Presentation Layer is also called the Translation layer. It receives data from application layer, which is the form of characters and numbers. Presentation layer translates converts this data to machine understandable binary format, after which it compresses the data and encrypts it. Compression of data makes data transmission faster, and encryption makes the data secure. At the sender's end the data is encrypted and at the receiver's end the data is decrypted.

The functions of the Presentation Layer are:
1. Translation
: For example, ASCII to EBCDIC
2. Encryption/Decryption
: Data encryption translates the data into another form or code. The encrypted data is known as the cipher test and the decrypted data is known as the plain text. A key value is used for encrypting and decrypting data.
3. Compression
: Number of bits for transmission are reduced which improve speed of transmission.

### Session Layer
This layer is responsible for setting up, and managing the connection followed by the termination of the connection. This layer also ensures security of the connection.

The functions of the Session Layer are:
1. Setting up, managing and termination of the session
: This layer uses APIs(Application Programming Interface) like NETBIOS to communicate between machines. An authentication is done before every session by the server. After authentication sever checks the authorization of the user to check if the user has the permissions to use the files. Session layer also keep a track of file that are downloaded from a server.
2. Synchronization
: This layer allows process to mark synchronization points or checkpoints in laymen's term. These points allow error identification so that the data can be re-synchronized, and data losses can be avoided.
3. Dialog Controller
: The session layer allows two systems whether to communicate in full-duplex or half-duplex.   

All the layer above session layer are known as __upper layers__ or __Software Layers__.

### Transport Layer
Transport layer is also known as the heart of OSI Model. It takes services from the network layer and provides services to the application layer. Data in this layer is known as *Segments*. The responsibility of End to End Delivery of the complete message is on this layer. Transport layer also provides the acknowledgement of the successful data transmission and if an error is found in the data it re-transmits the data. 
* At sender's side
: Transport layer receives data in formatted form from the upper layers, performs **Segmentation** and implements **Flow and Error control** to ensure proper data transmission. Port number of Source and Destination are added to its header and the segmented data is forwarded to the network layer.
* At receiver's side
: Port number is read from the header and the data is forwarded to the respective application. Transport layer also performs sequencing and reassembling of the segmented data.

The functions of the Transport Layer are:
1. Segmentation and reassembly
: In segmentation the data received from the session layer is divided into small data units called *Segments*. Each segment contains the port number of source and the destination, and a sequence number. Port numbers help to direct each segment to correct application and sequence number helps in reassembling the segments in correct order to give a meaningful message.
2. Flow Control
: Transport layer controls the amount of data being transmitted. For example - If a mobile(which can receive data @10mbps) is connected to a server(which can send data @100mbps), then the mobile, with the help of transport layer tells the server to send data @10mbps so as to maintain the performance and to avoid data loss.
3. Error Control
: If some data segment is missing at the receiver end transport layer uses **Automatic Repeat Request** schemes to re-transmit the lost or corrupt data. A group of bits called checksum is added to each segment to find out the received corrupted segment.

The services of the Transport Layer:
1. Connection-oriented Transmission
: It is done over TCP(Transmission Control Protocol). TCP provide a feedback whether the complete data is received or not and hence the lost data can be re-transmitted. Therefore, it is slower, and used where the transmission of whole data is must such as emails. 
2. Connectionless Transmission
: It is done over UDP(User Datagram Protocol). UDP does not provide any feedback whether the complete data is received or not. Therefore, it is faster, and used where the feedback does not matters such as online video streaming.

### Network Layer 
Network layer helps in the transmission of data from one machine to another in different networks.
The functions of the Network Layer are:
1. Logical Addressing
: IP addressing done in the network layer is known as the logical addressing. Each segment is provided the IP of both receiver and sender to form a packet. IP addresses are assigned to ensure that each data packet reaches it correct destination.
2. Routing
: It is a method of moving data packets from the sender to the receiver and it is based on the logic address format of IPv4 and IPv6. Network layer also takes care of the selection of the shortest path to transmit the packet, from the number of routes available, this is known as packet routing.

### Data Link Layer
Data Link Layer receives data packets from the network layer. While the *logical addressing* was done at the **network layer**, *physical addressing* is done at the **data link layer** where **MAC addresses**(it is a 12-digit *alpha-numeric* number embedded in *NIC*(network interface card) of a computer by the manufacturer) of sender and receiver are assigned to each data packet to form a frame. Data unit in the data link layer is called frame.

The functions of the Data Link Layer are:
1. Framing
: It provides a way for a sender to transmit meaningful bits to the receiver. This can be achieved by attaching special bit patterns to the beginning and end of the frame.
2. Access Control
: When a single communication channel is shared by multiple devices, data link layer helps to determine which device has control over the channel at a given time.
1. Physical Addressing
2. Error Control
3. Flow Control

Switch and Bridge are Data Link Layer devices.

### Physical Layer
It is responsible for the actual physical connection between devices. It contains information in the form of bits. 

The functions of the physical layer are:
1. Bit Synchronisation
: It provides the synchronization of the bits by providing a clock. This clock controls both sender and receiver thus providing synchronization at bit level.
2. Bit Rate Control
: The physical layer controls transmission(the number of bits sent per second).
3. Physical Topologies
: It specifies the way in which the different, devices/nodes are arranged in a network.
4. Transmission Mode
: It specifies the way in which the data flows between two devices. The various transmission modes consist of; Simplex, half-duplex and full-duplex.

Hub, Repeater, Modem, Cables are Physical Layer devices.

---

* Reference Links 
  * https://www.youtube.com/watch?v=vv4y_uOneC0
  * https://www.geeksforgeeks.org/layers-of-osi-model/